export const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)',
      boxShadow             : '0px -4px 12px rgba(23, 40, 83, 0.04)',
      borderRadius          : '8px',
      width                 : '606px',
      position              : 'relative'
    }
};